package navigation;

import actions.SetValues;
import common.start.Element;
import common.start.LocalVariables;
import common.start.StartParameters;

public class HomeSite {


    public static void pesquisarProduto(LocalVariables lv, StartParameters sttp, Element el) throws Exception {

        SetValues.setValores(sttp, lv, el, "campo pesquisa", "id", el.getCampopesquisa(), sttp.getTableLine()[0], "sendKeys", "Falha no preenchimento do campo nome");
        SetValues.setValores(sttp, lv, el, "botão pesquisa", "name", el.getBotaopesquisa(), "", "click", "Falha no preenchimento do campo nome");
        SetValues.setValores(sttp, lv, el, "selecionar produto", "xpath", el.getSelecionarproduto(), "", "click", "Falha no preenchimento do campo nome");
    }
}
