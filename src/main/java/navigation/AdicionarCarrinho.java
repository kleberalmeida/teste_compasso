package navigation;

import actions.SetValues;
import common.start.Element;
import common.start.LocalVariables;
import common.start.StartParameters;

public class AdicionarCarrinho {

    public static void adicionarCarrinho(LocalVariables lv, StartParameters sttp, Element el) throws Exception {

        SetValues.setValores(sttp,lv,el,"adicionar produto no carrinho","name",el.getAdicionarprodutonocarrinho(),"","click","Falha no preenchimento do campo nome");
        Thread.sleep(3000);
        SetValues.setValores(sttp,lv,el,"continuar para o carrinho","xpath",el.getContinuarparaocarrinho(),"","click","Falha no preenchimento do campo nome");
   }
}
