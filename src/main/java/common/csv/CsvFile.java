package common.csv;

import com.opencsv.CSVReader;
import common.start.StartParameters;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class CsvFile {

    public static void readFileCsv(StartParameters sttp) throws FileNotFoundException {

        CSVReader reader = new CSVReader(new FileReader(sttp.getProp().getProperty("filedata")),';');
        sttp.setReader(reader);
    }
}
