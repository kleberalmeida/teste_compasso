package common.utils;

import common.start.Element;

import java.io.IOException;

public class MapearElementos {

    public static void elementos (Element el) throws IOException, InterruptedException {


        el.setCampopesquisa("search_query_top");
        el.setBotaopesquisa("submit_search");
        el.setSelecionarproduto("//div[2]/h5/a");
        el.setAdicionarprodutonocarrinho("Submit");
        el.setContinuarparaocarrinho("//a[@title='Proceed to checkout']");
        el.setNomedoprodutonocarrinho("//td[2]/p/a");
        el.setValorproduto("product_price_1_1_0");

    }
}
