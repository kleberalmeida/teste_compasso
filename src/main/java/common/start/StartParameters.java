package common.start;

import com.opencsv.CSVReader;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.util.Properties;

public class StartParameters {

    private WebDriver driver;
    private Properties prop;
    private String dateNow;
    private File dir;
    private File dir2;
    private CSVReader reader;
    private String[] tableLine;
    private boolean condition;

    public boolean getCondition() {
        return condition;
    }

    public void setCondition(boolean condition) {
        this.condition = condition;
    }

    public String[] getTableLine() {
        return tableLine;
    }

    public void setTableLine(String[] tableLine) {
        this.tableLine = tableLine;
    }

    public CSVReader getReader() {
        return reader;
    }

    public void setReader(CSVReader reader) {
        this.reader = reader;
    }

    public File getDir2() {
        return dir2;
    }

    public void setDir2(File dir2) {
        this.dir2 = dir2;
    }

    public String getDateNow() {
        return dateNow;
    }

    public File getDir() {
        return dir;
    }

    public void setDir(File dir) {
        this.dir = dir;
    }

    public void setDateNow(String dateNow) {
        this.dateNow = dateNow;
    }

    public Properties getProp() {
        return prop;
    }

    public void setProp(Properties prop) {
        this.prop = prop;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }
}
