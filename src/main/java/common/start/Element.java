package common.start;

public class Element {


    private String campopesquisa;
    private String botaopesquisa;
    private String selecionarproduto;
    private String adicionarprodutonocarrinho;
    private String continuarparaocarrinho;
    private String nomedoprodutonocarrinho;
    private String valorproduto;

    public String getCampopesquisa() {
        return campopesquisa;
    }

    public void setCampopesquisa(String campopesquisa) {
        this.campopesquisa = campopesquisa;
    }

    public String getBotaopesquisa() {
        return botaopesquisa;
    }

    public void setBotaopesquisa(String botaopesquisa) {
        this.botaopesquisa = botaopesquisa;
    }

    public String getSelecionarproduto() {
        return selecionarproduto;
    }

    public void setSelecionarproduto(String selecionarproduto) {
        this.selecionarproduto = selecionarproduto;
    }

    public String getAdicionarprodutonocarrinho() {
        return adicionarprodutonocarrinho;
    }

    public void setAdicionarprodutonocarrinho(String adicionarprodutonocarrinho) {
        this.adicionarprodutonocarrinho = adicionarprodutonocarrinho;
    }

    public String getContinuarparaocarrinho() {
        return continuarparaocarrinho;
    }

    public void setContinuarparaocarrinho(String continuarparaocarrinho) {
        this.continuarparaocarrinho = continuarparaocarrinho;
    }

    public String getNomedoprodutonocarrinho() {
        return nomedoprodutonocarrinho;
    }

    public void setNomedoprodutonocarrinho(String nomedoprodutonocarrinho) {
        this.nomedoprodutonocarrinho = nomedoprodutonocarrinho;
    }

    public String getValorproduto() {
        return valorproduto;
    }

    public void setValorproduto(String valorproduto) {
        this.valorproduto = valorproduto;
    }
}
