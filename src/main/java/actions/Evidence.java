package actions;

import common.start.LocalVariables;
import common.start.StartParameters;
import common.utils.CaptureScreen;

import java.io.IOException;

public class Evidence {

    public static void evidenceResult(StartParameters sttp, LocalVariables lv) throws IOException {

        if(sttp.getCondition()) {
            CaptureScreen.capturePage(sttp, lv);

        }else {
            CaptureScreen.capturePage(sttp, lv);
        }
    }
}
