package actions;

import common.start.LocalVariables;
import common.start.StartParameters;
import org.openqa.selenium.By;

public class ValidateElementExistence {

    public static boolean verifyByName(StartParameters sttp, LocalVariables lv) throws InterruptedException {

        for (int i = 0; i < 60; i++) {
            try {
                sttp.getDriver().findElement(By.name(lv.getRobotValueAttribute()));
                return true;

            } catch (Throwable e) {
                Thread.sleep(1000);
            }


            }

        return false;
    }

    public static boolean verifyById(StartParameters sttp, LocalVariables lv) throws InterruptedException {

        for (int i = 0; i < 60; i++) {
            try {
                sttp.getDriver().findElement(By.id(lv.getRobotValueAttribute()));
                return true;

            } catch (Throwable e) {
                Thread.sleep(1000);
            }


        }

        return false;
    }

    public static boolean verifyByXpath(StartParameters sttp, LocalVariables lv) throws InterruptedException {

        for (int i = 0; i < 60; i++) {
            try {
                sttp.getDriver().findElement(By.xpath(lv.getRobotValueAttribute()));
                return true;

            } catch (Throwable e) {
                Thread.sleep(1000);
            }


        }

        return false;
    }

    public static boolean verifyByCss(StartParameters sttp, LocalVariables lv) throws InterruptedException {

        for (int i = 0; i < 60; i++) {
            try {
                sttp.getDriver().findElement(By.cssSelector(lv.getRobotValueAttribute()));
                return true;

            } catch (Throwable e) {
                Thread.sleep(1000);
            }


        }

        return false;
    }

    public static boolean verifyByParcialLink(StartParameters sttp, LocalVariables lv) throws InterruptedException {

        for (int i = 0; i < 60; i++) {
            try {
                sttp.getDriver().findElement(By.partialLinkText(lv.getRobotValueAttribute()));
                return true;

            } catch (Throwable e) {
                Thread.sleep(1000);
            }


        }

        return false;
    }

    public static boolean verifyByLinkText(StartParameters sttp, LocalVariables lv) throws InterruptedException {

        for (int i = 0; i < 60; i++) {
            try {
                sttp.getDriver().findElement(By.linkText(lv.getRobotValueAttribute()));
                return true;

            } catch (Throwable e) {
                Thread.sleep(1000);
            }

        }

        return false;
    }
}
